# Flatland

A very rudimentary (and **slow**) library for painting pixels on a screen. Uses brute-force to paint a pixel-by-pixel array onto your screen.


Provides 4 things:

1. A **screen** -- either through Glutin & OpenGL (*window backend*) or Crossterm (*terminal backend*)
2. An **event handler** -- glutin-style input catching using a closure.
3. (optional) A **canvas** object for getting and setting pixels a little easier.
4. (option) A **pixel** object so you're not manually writing BGRA pixel splices everywhere.


This is a very slow library intended for small textures or things that don't need to refresh every frame. The terminal backend is painfully slow, and if running frame-by-frame you'll notice heavy input lag. 


The library expects all inputs as flat arrays of tightly packed BGRA values -- so a 4x4 array of pixels would look like:


```rust
    [
     // B  G  R  A      B  G  R  A      ...             ...
        0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,
        0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,
        0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,
        0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255,   0, 0, 0, 255
    ]

```


You can pump slices like the one above into a **screen**'s **draw** function and see the results instantly.


Alternatively, you can use a **canvas** to manipulate things slightly easier. The **canvas** is just a light wrapper around serialized pixel array with some helper functions.


Check out the examples to get a feel for things!


### TODO

- [x] Implement Canvas / Backends in BGRA instead of RGB (better for cpu apparently)
- [ ] Implement other Pixel structs (Rgba, Bgra)
- [ ] Implement proper error handling
- [ ] Implement bounds checking for canvas / draw functions
- [ ] Implement Drop operations for handlers / backends
- [ ] Write proper cargo documentation
