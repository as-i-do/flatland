use flatland::screens::{Renderer, window::Screen};
use flatland::eventhandlers::{EventHandler, Event, Key};
use flatland::canvas::Canvas;
use flatland::pixel::RGB;
use std::{thread::sleep, time::{Instant, Duration}};
const WIDTH: usize = 64;
const HEIGHT: usize = 32;


fn main(){
    let (mut screen, mut events) = Screen::create(WIDTH, HEIGHT).unwrap();
    let mut running = true;
    let mut direction = true;
    let mut color = 0;
    let frame_length = Duration::from_millis(18);
    let mut last_frame = Instant::now();
    let mut canvas = Canvas::new(WIDTH, HEIGHT);
    while running{
        match direction {
            true => color = color + 5,
            false => color = color -5,
        }

        events.catch(|event| {
            match event{
                Event::Close => running = false,
                Event::Resize(x,y) => screen.resize(x, y).unwrap(),
                Event::KeyPress(c) => match c {
                    Key::A => running = false,
                    Key::Q => direction = !direction,
                    _ => ()
                },
                _ => ()
            }
        });
        

        match color {
            255 => direction = false,
            0 => direction = true,
            _ => ()
        }

        let color = RGB::new(color, color, color);
        canvas.fill(0,0,10, 10, &color.into()).unwrap();
        canvas.fill(54,22,10, 10, &color.into()).unwrap();
        canvas.fill(54,0,10, 10, &color.into()).unwrap();
        canvas.fill(0,22,10, 10, &color.into()).unwrap();
        screen.draw(canvas.as_mut_slice()).unwrap();

        // Do Framerate
        let elapsed = last_frame.elapsed();
        if elapsed < frame_length {
            sleep(frame_length - elapsed);
        }
        last_frame = Instant::now();
    }
}
