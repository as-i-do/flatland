use crate::FlatResult as Result;

pub struct Canvas{
    width: usize,
    height: usize,
    pixels: Vec<u8>
}

impl Canvas{
    pub fn new(width: usize, height: usize) -> Canvas{
        let mut pixels = Vec::with_capacity(width*height*4);
        for _ in 0..width*height{
            pixels.push(0);
            pixels.push(0);
            pixels.push(0);
            pixels.push(255);
        }
        return Canvas{
            width,
            height,
            pixels
        }
    }

    pub fn write(
        &mut self, x: usize, y: usize,
        width: usize, height: usize,
        data: &[u8]
    ) -> Result<()>{
        let topleft = (y * self.width * 4) + (x * 4);
        for i in 0..height{
            let rowstart = topleft + (i * self.width * 4);
            for j in (0..width*4).step_by(4){
                let point = rowstart + j;
                let datapoint = (i*width*4) + j;
                self.pixels[point] = data[datapoint];
                self.pixels[point+1] = data[datapoint + 1];
                self.pixels[point+2] = data[datapoint + 2];
                self.pixels[point+3] = data[datapoint + 3];
            }
        }
        Ok(())
    }

    pub fn fill(&mut self, x: usize, y: usize,
        width: usize, height: usize,
        color: &[u8;4]
    ) -> Result<()>{
       let mut v = vec![];
       for _ in 0..width*height*4{
           v.push(color[0]);
           v.push(color[1]);
           v.push(color[2]);
           v.push(color[3]);
       }
       return self.write(x, y, width, height, v.as_slice())
    }

    pub fn overwrite(&mut self, data: &[u8]) -> Result<()>{
        self.pixels.splice(0..self.width*self.height*4, data.iter().cloned());
        Ok(())
    }

    pub fn clearall(&mut self){
        self.overwrite(vec![0; self.width*self.height*4].as_slice()).unwrap();
    }
    pub fn clear(&mut self, x:usize, y:usize, width:usize, height:usize) -> Result<()>{
        self.write(x, y, width, height, vec![0; width*height*4].as_slice()) 
    }

    pub fn as_slice(&self) -> &[u8]{
        return self.pixels.as_slice()
    }

    pub fn as_mut_slice(&mut self) -> &[u8]{
        return self.pixels.as_mut_slice()
    }

    pub fn get_pixel(&self, x: usize, y: usize) -> &[u8]{
        let pixel = (y*self.width + x) * 4;
        &self.pixels[pixel..pixel+3]
    }
    pub fn set_pixel(&mut self, x: usize, y: usize, data: &[u8;4]) -> Result<()>{
        let pixel = (y*self.width + x) * 4;
        self.pixels.splice(pixel..pixel+4, data.iter().cloned());
        Ok(())
    }
}
