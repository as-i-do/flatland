extern crate minifb;
use std::marker::PhantomData;
use crate::FlatResult as Result;
use crate::screens::Renderer;
use crate::eventhandlers::framebuf::Handler;
use minifb::{Window, Key, Scale, WindowOptions};


pub struct Screen<'a>{
    width: usize,
    height: usize,
    screen: Window,
    phantom: PhantomData<&'a Window>
}


impl<'a> Renderer for Screen<'a>{
    type Backend = Screen<'a>;
    type EventHandler = Handler<'a>;

    fn create(width: usize, height: usize) -> Result<(Screen<'a>, Handler<'a>)>{
        let screen = match Window::new("TEST", width, height, WindowOptions{..WindowOptions::default()}){
            Ok(win) => win,
            Err(err) => panic!("Could not make screen: {}", err)
        };
        let events = Handler::new(&screen);
        
        Ok((Screen{
            width,
            height,
            screen,
            phantom: PhantomData
        }, events))
    }

    fn draw(&mut self, data: &[u8]) -> Result<()>{
        let convert: Vec<u32> = data.iter().map(|x| *x as u32).collect();
        self.screen.update_with_buffer(convert.as_slice()).unwrap();
        Ok(())
    }

    fn resize(&mut self, width: f64, height: f64) -> Result<()>{
        Ok(())
    }

    fn refresh(&mut self){}
}

