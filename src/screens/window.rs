extern crate gl;
extern crate crossterm;
use crate::FlatResult as Result;
use crate::screens::Renderer;

use crate::eventhandlers::window::Handler;
use glutin::{GlContext};
use std::ffi::CString;
use std::mem::size_of;

pub struct Screen{
    width: usize,
    height: usize,
    context: glutin::GlWindow,
    program: u32,
    vao: u32,
    tbo: u32,
}

const DEFAULT_VERTEX_SHADER: &str = r#"
#version 330 core
layout (location=0) in vec2 aPos;
layout (location=1) in vec2 aTex;

out vec2 texCoord;

void main(){
    gl_Position = vec4(aPos.x, aPos.y, 0.0, 1.0);
    texCoord = aTex;
}"#;

const DEFAULT_FRAGMENT_SHADER: &str = r#"
#version 330 core
in vec2 texCoord;
out vec4 FragColor;

uniform sampler2D ourTexture;

void main(){
    FragColor = texture(ourTexture, texCoord);
}"#;

const F32SIZE: usize =  size_of::<f32>();
const WINDOW_WIDTH: f64 = 800.0;
const WINDOW_HEIGHT: f64 = 600.0;

fn compile_shader(src: &str, ty:u32) -> u32 {
    unsafe{
        let shad: u32 = gl::CreateShader(ty);
        let c_src = CString::new(src).unwrap();
        gl::ShaderSource(shad, 1, &c_src.as_ptr(), std::ptr::null());
        gl::CompileShader(shad);

        // Let's check if we succeeded...
        let mut status: i32 = 0;
        gl::GetShaderiv(shad, gl::COMPILE_STATUS, &mut status);
        match status as u8{
            gl::FALSE => {
                let mut log = Vec::with_capacity(512);
                gl::GetShaderInfoLog(
                    shad,
                    512,
                    std::ptr::null_mut(),
                    log.as_mut_ptr()
                );
                panic!("{:?}", CString::from_raw(log.as_mut_ptr()));
            },
            _ => ()
        };
        shad
    }
}
impl Renderer for Screen{
    type Backend = Screen;
    type EventHandler = Handler;

    fn create(width: usize, height: usize) -> Result<(Screen, Handler)>{
        std::env::set_var("WINIT_UNIX_BACKEND", "x11");
        let events_loop = glutin::EventsLoop::new();
        let window = glutin::WindowBuilder::new()
            .with_dimensions((WINDOW_WIDTH, WINDOW_HEIGHT).into());
        let context = glutin::ContextBuilder::new();
        let gl_window = glutin::GlWindow::new(
            window, context, &events_loop
        ).unwrap();
        unsafe {
            gl_window.make_current().unwrap();
        }
        gl::load_with(|symbol| gl_window.get_proc_address(symbol) as *const _);
        
        // Create our vertex information =====================================
        let verts: [f32; 16] = [
             // Pos      // Texture
            //-1.0,  1.0,  0.0, 1.0,
            // 1.0,  1.0,  1.0, 1.0,
            // 1.0, -1.0,  1.0, 0.0,
            //-1.0, -1.0,  0.0, 0.0,
            -1.0,  1.0,  0.0, 0.0,
             1.0,  1.0,  1.0, 0.0,
             1.0, -1.0,  1.0, 1.0,
            -1.0, -1.0,  0.0, 1.0,
        ];

        let indices: [u32; 6] = [
            0, 1, 2,
            2, 3, 0
        ];

        // Create and bind our vertex array object...
        let mut vao: u32 = 0;
        unsafe {
            gl::GenVertexArrays(1, &mut vao);
            gl::BindVertexArray(vao);
        }

        // Create a vertex buffer
        let mut vbo: u32 = 0;
        let mut ebo: u32 = 0;
        
        unsafe {
            gl::GenBuffers(1, &mut vbo);
            gl::BindBuffer(gl::ARRAY_BUFFER, vbo);
            gl::BufferData(
                gl::ARRAY_BUFFER,
                (verts.len() * F32SIZE) as isize,
                std::mem::transmute(&verts),
                gl::STATIC_DRAW
            );

            // And element buffer
            gl::GenBuffers(1, &mut ebo);
            gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo);
            gl::BufferData(
                gl::ELEMENT_ARRAY_BUFFER,
                (indices.len() * F32SIZE) as isize,
                std::mem::transmute(&indices),
                gl::STREAM_DRAW
            );

            // Define how we interpret our vertex information
            gl::VertexAttribPointer(
                0, // Which attrib to configure (location=0)
                2, // Size of vertex attribut (vec2)
                gl::FLOAT, // Datatype
                gl::FALSE as u8, // Do we want to normalize int data?
                4 * F32SIZE as i32, // stride. Zero works for 'tightly packed' vertex attributes.
                std::ptr::null(), //offset
            );

            // Finally, enable this as location(0)
            gl::EnableVertexAttribArray(0);
        }

        // Create our texture ================================================
        let mut tbo: u32 = 0; 
        unsafe {
            gl::GenTextures(1, &mut tbo);
            gl::BindTexture(gl::TEXTURE_2D, tbo);

            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_S, gl::CLAMP_TO_BORDER as i32);  
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_WRAP_T, gl::CLAMP_TO_BORDER as i32);
            // We'll linearly scale down (blurs)
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR as i32);
            // But we'll use nearest to scale up (Pixelly!)
            gl::TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::NEAREST as i32);
            // Does something to the way we read pixels!
            gl::PixelStorei(gl::UNPACK_ALIGNMENT, 1);
            
            let tex_size = width * height * 4;
            let mut temp_tex: Vec<u8> = vec![0; tex_size];
            gl::TexImage2D(
                gl::TEXTURE_2D, // Texture target space
                0, // mimap level
                gl::RGBA as i32, // Color Space
                width as i32, // width
                height as i32, //height
                0, // Always zero
                gl::BGRA, // Format of raw data,
                gl::UNSIGNED_BYTE,
                temp_tex.as_mut_ptr() as *mut std::ffi::c_void,
            );

            // Define how we read the texture info from our vertices
            gl::VertexAttribPointer(
                1, 2, gl::FLOAT, gl::FALSE as u8,
                4 * F32SIZE as i32,
                (2 * F32SIZE) as *const _
            );
            gl::EnableVertexAttribArray(1);
        }

        // Prepare program / Shaders =========================================
        let program;
        unsafe {
            let vshad = compile_shader(DEFAULT_VERTEX_SHADER, gl::VERTEX_SHADER);
            let fshad = compile_shader(DEFAULT_FRAGMENT_SHADER, gl::FRAGMENT_SHADER);
            program = gl::CreateProgram();
            gl::AttachShader(program, vshad);
            gl::AttachShader(program, fshad);
            
            gl::LinkProgram(program);
            gl::DeleteShader(vshad);
            gl::DeleteShader(fshad);
            gl::UseProgram(program);
            gl::BindVertexArray(vao);
            gl::ClearColor(0.0, 0.0, 0.0, 1.0);
        }

        let events = Handler::new(events_loop);
        let mut screen = Screen{
            context:gl_window,
            width,
            height,
            vao,
            program,
            tbo,
        };
        screen.resize(WINDOW_WIDTH, WINDOW_HEIGHT).unwrap();
        Ok((screen, events))
    }

    fn draw(&mut self, data: &[u8]) -> Result<()>{
        unsafe{
            let mut dat = vec![0; data.len()];
            dat.clone_from_slice(data);
            gl::UseProgram(self.program);
            gl::BindVertexArray(self.vao);
            gl::BindTexture(gl::TEXTURE_2D, self.tbo);
            gl::TexSubImage2D(
                gl::TEXTURE_2D, // Texture target space
                0, // mimap level
                0, 0, //(Offsets)
                self.width as i32, // width
                self.height as i32, //height
                gl::BGRA, // Format of raw data,
                gl::UNSIGNED_INT_8_8_8_8_REV,
                dat.as_mut_ptr() as *mut std::ffi::c_void,
            );
            gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, std::ptr::null());
        }
        self.context.swap_buffers().unwrap();
        Ok(())
    }
    fn resize(&mut self, width: f64, height: f64) -> Result<()>{
        let dpi = self.context.get_hidpi_factor();
        let (width, height) = (width*dpi, height*dpi);
        let win_aspect = width/height;
        let tex_aspect = (self.width as f64) / (self.height as f64);
        let mut tex_width = width;
        let mut tex_height = height;

        match win_aspect {
            n if n > tex_aspect => {
               tex_width = tex_aspect * tex_height 
            },
            _ => {
                tex_height = tex_width / tex_aspect
            } 
        }

        let tex_offset_x = (width - tex_width) / 2.0;
        let tex_offset_y = (height - tex_height) / 2.0;

        unsafe{
            gl::Viewport(
                tex_offset_x as i32,
                tex_offset_y as i32,
                tex_width as i32,
                tex_height as i32
            );
        }
        Ok(())
    }

    fn refresh(&mut self){
        unsafe{
            gl::DrawElements(gl::TRIANGLES, 6, gl::UNSIGNED_INT, std::ptr::null());
        }
        self.context.swap_buffers().unwrap();
    }
}
