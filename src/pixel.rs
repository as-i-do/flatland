use std::vec::Vec;

pub trait Pixel {
    fn raw(&self) -> [u8;4];
    fn red(&self) -> u8;
    fn green(&self) -> u8;
    fn blue(&self) -> u8;
    fn alpha(&self) -> u8;
}

#[derive(Clone, Copy)]
pub struct RGB {
    r: u8,
    g: u8,
    b: u8,
}

impl RGB {
    pub fn new(r: u8, g:u8, b:u8) -> RGB{
       RGB{r, g, b}
    }
}

impl Pixel for RGB {
    fn raw(&self) -> [u8;4]{
        [self.b, self.g, self.r, 255]
    }
    fn red(&self) -> u8{
        self.r
    }
    fn green(&self) -> u8{
        self.g
    }
    fn blue(&self) -> u8{
        self.b
    }
    fn alpha(&self) -> u8{
        255
    }
}

impl From<&[u8]> for RGB{
    fn from(splice: &[u8]) -> RGB{
        RGB::new(splice[2], splice[1], splice[0])
    }
}

impl Into<[u8;4]> for RGB{
    fn into(self) -> [u8;4]{
        self.raw()
    }
}
