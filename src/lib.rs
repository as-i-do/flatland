pub mod screens;
pub mod eventhandlers;
pub mod canvas;
pub mod pixel;
// Define a nice simple error to use...
use std::error::Error;
pub type FlatResult<T> = Result<T, Box<Error>>;
#[cfg(test)]
mod tests {
    use crate::renderers::basic::BasicRenderer;
    #[test]
    fn basic() {
        BasicRenderer::create(100,100).unwrap();
    }
}
